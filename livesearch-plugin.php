<?php
/**
 * Plugin Name: Livesearch GNews
 * Description: Use this shortcode to create a Ajax powered search field that returns current Google News headlines on your page. Use the shortcode [livesearch-gnews] Your Title Text [/livesearch-gnews] to add to page.
 * Version: 1.0
 * Author: Dru Martin
 * Author URI: http://drumartinshead.com
 */

function livesearch_plugin_scripts() // $handle, $src, $deps, $ver, $in_footer
{
	// Register the script
	wp_register_script( 'livesearch-script', plugins_url( '/assets/js/livesearch.js', __FILE__ ), array( 'jquery' ), '20101021', true );
	wp_register_style( 'livesearch-style', plugins_url( '/assets/sass/style.css', __FILE__ ), array(), '20101021', 'all' );
	wp_register_style('googleFonts', 'http://fonts.googleapis.com/css?family=PT+Sans:regular,bold', 'all'); //http://webdesignfromscratch.com/wordpress/using-google-web-fonts-with-wordpress-the-right-way/

	// Enqueue the scripts
	wp_enqueue_script( 'livesearch-script' );
	wp_enqueue_style( 'livesearch-style' );
	wp_enqueue_style( 'googleFonts');
}
add_action( 'wp_enqueue_scripts', 'livesearch_plugin_scripts' );


// create shortcode function
// http://www.smashingmagazine.com/2012/05/01/wordpress-shortcodes-complete-guide/

function livesearch_sc_func($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));

	$return_string = '<div id="main-livesearch">'; //wrapper
	if ($content != null && $content != '') {
		$return_string .= '<h1 class="title">' .$content. '</h1>'; //headline
	}
	$return_string .= '<div class="livesearch-input-wrap">'; // input wrap
	$return_string .= '<input type="text" id="search" autocomplete="off" onkeyup="showResult(this.value)">'; // search input
	$return_string .= '<span class="spinner"><div id="bowlG"><div id="bowl_ringG"><div class="ball_holderG"><div class="ballG"></div></div></div></div></span>'; //spinner
	$return_string .= '</div>'; // close input wrap
	$return_string .= '<ul id="livesearch"></ul>'; //show results
	$return_string .= '</div>'; // close #main wrapper

	wp_reset_query();
	return $return_string;
}
add_shortcode("livesearch-gnews", "livesearch_sc_func");


// use this to load the plugin scripts conditionally. This plugin isn't that heavy so maybe not.
// http://beerpla.net/2010/01/13/wordpress-plugin-development-how-to-include-css-and-javascript-conditionally-and-only-when-needed-by-the-posts/

//add_filter('the_posts', 'conditionally_add_scripts_and_styles'); // the_posts gets triggered before wp_head
//function conditionally_add_scripts_and_styles($posts){
//	if (empty($posts)) return $posts;
//
//	$shortcode_found = false; // use this flag to see if styles and scripts need to be enqueued
//	foreach ($posts as $post) {
//		if (stripos($post->post_content, '[code]') !== false) {
//			$shortcode_found = true; // bingo!
//			break;
//		}
//	}
//
//	if ($shortcode_found) {
//		// enqueue here
//		wp_enqueue_style('my-style', '/style.css');
//		wp_enqueue_script('my-script', '/script.js');
//	}
//
//	return $posts;
//}