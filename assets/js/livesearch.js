function showResult(str) {

    if (str == '') {
        document.getElementById("livesearch").innerHTML="";
        jQuery('.spinner, #livesearch').fadeOut();
        return;
    }
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        jQuery('.spinner, #livesearch').fadeIn();
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            document.getElementById("livesearch").innerHTML=xmlhttp.responseText;
        }
    }
    // TODO: the following path is fragile. Find a better way to reference the wp plugins directory.
    xmlhttp.open("GET", "/wp-content/plugins/livesearch-plugin/assets/framework/livesearch.php?q="+str,true);
    xmlhttp.send();
}


