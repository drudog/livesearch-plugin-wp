<?php

$crossDXmlDoc = new SimpleXMLElement('https://news.google.com/news/feeds?q=&output=rss', NULL, TRUE);
$xmlDoc = dom_import_simplexml($crossDXmlDoc);

$x=$xmlDoc->getElementsByTagName('item');

//get the q parameter from URL
$q=$_GET["q"];

//lookup all links from the xml file if length of q>0
if (strlen($q)>0) {
	$hint="";
	for($i=0; $i<($x->length); $i++) {
		$y=$x->item($i)->getElementsByTagName('title');
		$z=$x->item($i)->getElementsByTagName('link');
		if ($y->item(0)->nodeType==1) {
			//find a link matching the search text
			if (stristr($y->item(0)->childNodes->item(0)->nodeValue,$q)) {
				if ($hint=="") {
					$hint="<li><a href='" .
					      $z->item(0)->childNodes->item(0)->nodeValue .
					      "' target='_blank'>" .
					      $y->item(0)->childNodes->item(0)->nodeValue . "</a></li>";
				} else {
					$hint=$hint . "<li><a href='" .
					      $z->item(0)->childNodes->item(0)->nodeValue .
					      "' target='_blank'>" .
					      $y->item(0)->childNodes->item(0)->nodeValue . "</a></li>";
				}
			}
		}
	}
}

// Set output to "no suggestion" if no hint was found
// or to the correct values
if ($hint=="") {
	$response="<span class='nosuggest'>no results found</span>";
} else {
	$response=$hint;
}

//output the response
echo $response;
?>
