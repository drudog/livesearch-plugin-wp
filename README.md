# README #

Install this WP plugin just as you would any other plugin and activate. Usage instructions are in the plugin description.

ONE NOTE: this plugin's directory needs to be renamed as 'livesearch-plugin' until the file path is fixed in livesearch.js


### v2 Plugin TODO Notes: ###

- Move ‘keyup’ function out of HTML and into js
- create SASS variables for easy theming
- search the entire description text of the news items as well as titles/links.
- cancel the search with ESC or by clicking anywhere on the page.
- fix fragile file path in livesearch.js